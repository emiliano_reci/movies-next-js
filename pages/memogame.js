import Base from './../layouts/base'
import Tablero from './../components/Tablero'
import Header from './../components/Header'
import construirBaraja from './../utils/construirBaraja'
import Head from 'next/head'    
import Confetti from 'react-confetti'

const getEstadoInicial= () => {
    const baraja= construirBaraja();
    return{
      baraja,
      parejaSeleccionada: [],
      estaComparando: false,
      numeroDeIntentos: 0,  
    };
  }

export default class extends React.Component {
    constructor(props){
        super(props);
        this.state=  {
        baraja: construirBaraja(),
        parejaSeleccionada: [],
        estaComparando: false,
        numeroDeIntentos: 0,
        runConfetti:false,
        confetti: 0
        }
    }
    seleccionarCarta(carta){
        if( this.state.estaComparando ||
          this.state.parejaSeleccionada.indexOf(carta) > -1 ||
          carta.fueAdivinada
        ) {
          return;
        }
        const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];
        this.setState({
          parejaSeleccionada
        });
        if(parejaSeleccionada.length === 2){
          this.compararPareja(parejaSeleccionada);
        }
      }
    compararPareja(parejaSeleccionada){
        this.setState({
          estaComparando: true
        });
        
    setTimeout( () => {
        const [primeraCarta, segundaCarta] = parejaSeleccionada;
        let baraja = this.state.baraja;

        if( primeraCarta.icono === segundaCarta.icono){
          baraja = baraja.map((carta) => {
            if(carta.icono !== primeraCarta.icono){
              return carta;
            }
            return {...carta, fueAdivinada: true}
          });
        } 
        this.verificarSiHayGanador(baraja);
    
        this.setState({
          parejaSeleccionada: [],
          baraja,
          estaComparando: false,
          numeroDeIntentos: this.state.numeroDeIntentos + 1
        });
        }, 1000)  
    }
    verificarSiHayGanador(baraja) {
        if (
          baraja.filter((carta) => !carta.fueAdivinada).length === 0
        ) {
          //alert(`Ganaste en ${this.state.numeroDeIntentos} intentos!`);
          this.setState({
            runConfetti:true,
            confetti: 1
          })
        }
    }
    resetearPartida() {
      this.setState(
        getEstadoInicial()
      );
      this.setState({
        confetti: false
      })
    }
    render(){
        return(
            <div className="dic">
            <Confetti
              height={660}
              width={1340} 
              opacity={this.state.confetti}
              run={this.state.runConfetti} 
            />
              <Base>
                <Head>
                  <title>MOVIES-GAME</title>
                  <link rel="icon" type="image/x-icon" href="/static/movie.png" />
                  <link href="/static/index.css" rel="stylesheet" />

                </Head>
              <Header
                    numeroDeIntentos={this.state.numeroDeIntentos}
                    resetearPartida={() => this.resetearPartida()} 
                />
                <Tablero className="Tablero"
                  baraja={this.state.baraja }
                  parejaSeleccionada={this.state.parejaSeleccionada}
                  seleccionarCarta={(carta) => this.seleccionarCarta(carta)}
                />
              </Base>    
            </div>
        )
    }
    
}