import Head from 'next/head'
import Base from './../layouts/base'

//import WOW from 'wowjs'

export default class extends React.Component {
    //implementacion de awesome WOW js
    componentDidMount() {
        if(typeof window !== 'undefined') {
          window.WOW = require('wowjs');
        }
        new WOW.WOW().init();
      }
    render(){
        return(
            <Base>
                <Head>
                    <title>MOVIES-GAME</title>
                    <link href="/static/index.css" rel="stylesheet" />
                    <link rel="icon" type="image/x-icon" href="/static/movie.png" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
                    <link rel="stylesheet" href="css/animate.css" />
                </Head>

                <div>
                    <h1 className="wow lightSpeedIn" 
                    data-wow-duration="4s"
                    style={{color:'yellow', fontSize:50}}>
                        Bienvenido...
                    </h1>
                    <img src='./static/beats.png' className="wow fadeInDown" />
                    <img src='./static/beats.png' className="wow fadeInDown" data-wow-delay="0.2s"/>
                    <img src='./static/beats.png' className="wow fadeInDown" data-wow-delay="0.4s"/>
                    <img src='./static/beats.png' className="wow fadeInDown" data-wow-delay="0.95s"/>
                    <img src='./static/beats.png' className="wow fadeInDown" data-wow-delay="1.2s"/>
                </div>            
            </Base>
        )
    }
}





