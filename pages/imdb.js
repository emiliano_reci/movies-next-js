import Head from 'next/head'
import Base from './../layouts/base'
import axios from 'axios'
import Pelicula from './../components/pelicula'
import Buscador from './../components/Buscador'

const URL= 'http://www.omdbapi.com/?apikey=ced989f5&s='

export default class extends React.Component {
    constructor(props){
        super(props);
        this.state=  {
        buscador:'',
        showMovies: false,
        pel:[], 
        numeroDePeliculas: 0,
        pagina: 1,
        paginaInicio:1
    }
}
    handleData = (data) => {
        this.setState({
            buscador: data,
            page: 1,
            pageInicio:1,
            pagina:1
      })
      this.getMovies(this.state.paginaInicio, data)
    }
 
    getMovies =(pageInicio, data) =>{   
        const { buscador, pagina } =this.state;
        //console.log('pagina', pagina)
        axios.get(`${URL}${data}&page=${pageInicio}`)
        .then(res => {
          const response = res.data;
          //console.log(res.data.Search)
          if(response.Search !=null){
            const pel = response.Search
            //console.log(pel)
            this.setState({
                pel: pel,
                numeroDePeliculas: res.data.totalResults
            })
                return pel
            } 
        })    
    }
    getMovies2 =() =>{   
        const { buscador, pagina } =this.state;
        console.log('pagina', pagina)
        axios.get(`${URL}${buscador}&page=${pagina}`)
        .then(res => {
          const response = res.data;
          //console.log(res.data.Search)
          if(response.Search !=null){
            const pel = response.Search
            //console.log(pel)
            this.setState({
                pel: pel,
                numeroDePeliculas: res.data.totalResults
            })
                return pel
            } 
        })    
    }
   
    irAPagina = (operacion) => {
        let { pagina } = this.state;
        pagina = pagina + operacion;
        this.setState({
            pagina,
        }, this.getMovies2)   
        console.log(`${URL}${this.state.buscador}&page=${pagina}`)
    }

    render(){
        let moviesSearch = null;
        let pagerSearch = null; 
        let inicio = null;
        const { pagina, numeroDePeliculas } = this.state;
            moviesSearch = (
                <div className="peliculas">
                    {this.state.pel.map((pelis) => <Pelicula {...pelis } key={pelis.imdbID}/> )} 
                </div>
            )
            pagerSearch = (
                <div className="pager">
                    {pagina > 1 && <div className="pageBefore" onClick={e => this.irAPagina(-1)}>Anterior </div>} 
                    {(pagina * 10) < numeroDePeliculas && <div className="pageNext" onClick={e => this.irAPagina(1)}>Siguiente</div>}
                </div>
            )
        return(
            <Base>
                <Head>
                    <title>MOVIES-GAME</title>
                    <link href="/static/index.css" rel="stylesheet" />
                </Head>
                <Buscador
                    handlerFromParant={this.handleData} 
                /> 
                <div className="peliculas">
                    {this.state.pagina}
                    { moviesSearch }
                    { pagerSearch }
                </div>
                
                <style jsx>{`
                 .peliculas{
                     color: red
                 }
                `}
                </style>
            </Base>            
        )
    }
}





  {/*
                pelis por pagina: {this.state.pel.length} 
                    total de pelis: {this.state.numeroDePeliculas}
                {this.props.peliculas.map((pelis) => <Pelicula {...pelis } key={pelis.imdbID}/> )}
                */}