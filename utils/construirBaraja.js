import ImagesArray from './imagesArray'
import shuffles from 'shuffle-array';
//import shuffle from 'lodash.shuffle';

const NUMERO_DE_CARTAS = 20;
export default () =>  {
  const cartas = [];
  const imagesArray = ImagesArray()
  while (cartas.length < NUMERO_DE_CARTAS) {
    const index = Math.floor(Math.random() * 10); 
    const carta = {
      icono: imagesArray.splice(index, 1)[0],
      fueAdivinada: false
    };
    //console. log('la carta tiene: ', carta)
    cartas.push(carta);
    cartas.push({...carta});
  }
  return shuffles(cartas);
  return cartas;
};


