import Carta from './Carta'
import shuffle from 'shuffle-array'

export default class Tablero extends React.Component {
    render() {
      return (
        <div className="tablero">
          {
            this.props.baraja
              .map((carta, index) => {
                const estaSiendoComparada = this.props.parejaSeleccionada.indexOf(carta) > -1;
                return<Carta
                  key={index} 
                  icono={carta.icono}
                  estaSiendoComparada={estaSiendoComparada}
                  seleccionarCarta={() => this.props.seleccionarCarta(carta)}
                  fueAdivinada={carta.fueAdivinada}
                />;
              })
          }
          <style jsx>{`
          .tablero{
            display: flex;
            flex-wrap: wrap;
            width: 700px;
            height: 600px;
            margin: 0 auto;
            justify-content: space-around;
            color: red;
          }
          `}
          </style>
        </div>
      ); 
    }
  };

