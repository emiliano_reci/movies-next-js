import ReactCardFlip from 'react-card-flip';
class Carta extends React.Component {

    render() {
        return (
            <div className="carta" onClick={this.props.seleccionarCarta }>
                <ReactCardFlip isFlipped={this.props.estaSiendoComparada || this.props.fueAdivinada}>
                    <div className="portada" key="front"></div>
                    <div className="contenido" key="back">
                        <div>
                            <img src={this.props.icono}  width='120px' height='115px'/>
                        </div>﻿
                    </div>
                </ReactCardFlip>          
                <style jsx>{`
                    .carta {
                        width: 125px;
                        height: 125px;
                    }
                    .portada {
                        width: 125px;
                        height: 125px;
                        background-color: #ffb300;
                    }
                    .portada:hover{
                        opacity: 0.5;
                        transition: 0.5s;
                    }
                    .contenido {
                        width: 125px;
                        height: 125px;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        background-color: rgb(3, 120, 144, 0.5);
                    }
                `}</style>
            </div>
        );
    }
}

export default Carta;
