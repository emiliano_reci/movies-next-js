export default class Header extends React.Component {
    render() {
      return (
        <header> 
            <div>
              <h3 className="marcador">
                Intentos: {this.props.numeroDeIntentos}
              </h3> 
              <button  
                style={{border:'none', color:'whitesmoke', fontWeight:'bolder'}}
                className="boton-reiniciar shadow" 
                onClick={this.props.resetearPartida}
              >
                Reiniciar
              </button>
          </div>
          <style jsx>{`
            .boton-reiniciar{
              position: absolute;
              top:40%;
              left:5%;
            }
            .marcador{
              position: absolute;
              left: 85%;
              color: red;
            }
            .shadow{
              width: 150px;
              height: 90px;
              background: linear-gradient(0deg,#000,#262626);
            }
            .shadow:before,
            .shadow:after{
                content: '';
                position: absolute;
                left: -2px;
                top: -2px;
                background: linear-gradient(45deg,#fb0094,#0000ff,#00ff00,#ffff00,#ff0000,#fb0094,#0000ff,#00ff00,#ffff00,#ff0000);
                background-size: 400%;
                width: calc(100% + 4px);
                height: calc(100% + 4px);
                z-index: -1;
                animation: animate 20s linear infinite;
            }
            .shadow:after{
                filter: blur(20px);
            }
            @keyframes animate{
                 0%
                 {background-position: 0 0;
                 }
                 50%
                 {background-position: 300% 0;
                 }
                 100%
                 {background-position: 0 0;
                 }
            }
            .button{
              background: linear-gradient(0deg,#000,#262626);
              color: white;
              width: 150px;
              height: 90px; 
              font-weight: bold
            }
          `}
          </style>
        </header>
      );
    }
  };