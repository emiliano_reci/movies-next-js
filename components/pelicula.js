export default ({ Title, Poster, Year}) => {
    //const titu = JSON.stringify(Title);
    
    const mostrarSplot = () => {
        alert(`Titulo: "${Title}", año: ${Year}`)
    }
//onClick={() => alert(`${Title}, ${Year}`)
    return(
        <div className="pelicula"
            onClick={ mostrarSplot }>
 
            <div className="poster"></div>
            <div className="titulo">{Title}</div>
            <style jsx>{`
                .pelicula{
                    height: 270px;
                    width: 210px;
                    display: flex;
                    flex-direction: column;
                    margin: 10px;
                }
                .titulo{
                    text-align: center;
                    width: 100%;
                    opacity: 0.9;   
                    background: black;
                    color: white;
                    font-size: 17px;
                }
                .poster{
                    flex: 1;
                    background-image: url(${Poster});
                    background-size: cover;
                    background-color: black;
                }
                .pelicula:hover{
                    opacity: 0.4;
                    transition: 0.5s;
                }
            `}
            </style>
        </div>
    )
}
