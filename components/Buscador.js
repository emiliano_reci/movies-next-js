class Buscador extends React.Component {
    state={
        valor:'',
        page:1
    }
    handleChange= (event) => {
        let valor1=event.target.value
        console.log(valor1)
        //console.log('desde handler...', this.state.valor1)
        this.setState({
            valor:valor1
        })

    }

    buscar= ()=>{
        console.log(this.state.valor, 'desde btn buscar')
        this.props.handlerFromParant(this.state.valor, this.state.page);
    }
    render() {
        return (
            <div>
                <input 
                    type="text" 
                    placeholder="ingrese el titulo de la pelicula" 
                    onChange={this.handleChange}
                />
                <button
                    onClick={this.buscar}                    
                >   Buscar
                </button>
            </div>
        );
    }
}

export default Buscador;

    
