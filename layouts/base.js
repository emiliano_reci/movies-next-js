import Link from 'next/link';

export default ({ children }) => (
    <div className="main">
        <div className="logo">
                <Link href="/">
                    <a>HOME</a>
                </Link> 
                {/*
                <Link href="/imdb">
                    <a> IMDB</a>
                </Link> 
                */}
                <Link href="/memogame">
                    <a> MEMOGAME</a>
                </Link>
        </div>
        {children}

        <style jsx>{`
            a{
                text-decoration: none;
                color: red;
                font-weight: bolder;
                margin: 5px
            }
            a:hover{
                opacity: 0.5;
                transition: 0.5s;
            }
        `}
        </style>
    </div>
)